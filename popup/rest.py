from rest_framework import serializers
from .models import UserNotification


class UserNotificationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = UserNotification
        fields = ('text', )

