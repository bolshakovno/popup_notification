from django.db import models
from django.contrib.auth.models import User


class UserNotification(models.Model):
    user = models.ForeignKey(User)
    added = models.DateTimeField(auto_now_add=True)
    text = models.TextField()