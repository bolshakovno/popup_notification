from django.conf import settings
from django.http import HttpResponse
from django.template import loader, RequestContext

from rest_framework import viewsets
from popup.models import UserNotification
from popup.rest import UserNotificationSerializer


def index(request):
    template = loader.get_template('index.html')
    context = RequestContext(request, {'extjs': settings.EXTJS})
    return HttpResponse(template.render(context))


class UserNotificationViewSet(viewsets.ModelViewSet):
    queryset = UserNotification.objects.all()
    serializer_class = UserNotificationSerializer
