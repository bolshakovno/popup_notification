Ext.application({
    name               : 'popup',
    appFolder          : '../static/popup',
    autoCreateViewport : true,

    controllers: [
        'popup.controller.PopupController'
    ]
});