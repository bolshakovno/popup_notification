Ext.define('popup.controller.PopupController', {
    extend: 'Ext.app.Controller',

    stores: [
        'Popup'
    ],

    wrapStyle: 'style="word-break: break-all; width: 500;"',

    init: function () {
        var me = this;

        me.control({
            'viewport > form': {
                afterrender: me.showPopup
            },

            'form > button': {
                click: function () {
                    me.showPopup(5)
                }
            }
        });
    },

    showPopup: function (limit) {
        var me = this;
        var store = Ext.getStore('Popup');

        store.load({
            url: 'popup/?limit=' + limit || 'popup',
            callback: function (records, operation, success) {
                Ext.each(records, function (i) {
                    Ext.create('widget.uxNotification', {
                        title: 'Сообщение',
                        position: 'br',
                        autoCloseDelay: 3000,
                        spacing: 20,
                        html: '<p ' + me.wrapStyle + '>' + i.data.text + '<p>'
                    }).show();
                });
            }
        })
    }

});
