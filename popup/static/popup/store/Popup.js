Ext.define('popup.store.Popup', {
    extend: 'Ext.data.Store',
    model: 'popup.model.Popup',
    autoLoad: false,
    proxy: {
        type: 'rest',
        limitParam: false,
        pageParam: false,
        startParam: false,
        url: 'popup/',
        reader: {
            type: 'json',
            root: 'results',
            successProperty: 'success'
        }
    }
});