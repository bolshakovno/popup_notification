Ext.define('popup.model.Popup', {
    extend: 'Ext.data.Model',
    fields: [
        {name: 'text', type: 'text'}
    ]
});