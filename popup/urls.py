from django.conf.urls import url, include
from rest_framework import routers
from popup.views import UserNotificationViewSet, index

router = routers.DefaultRouter()
router.register(r'popup', UserNotificationViewSet)

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
